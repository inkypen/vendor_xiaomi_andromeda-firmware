# vendor_xiaomi_andromeda-firmware

Firmware images for MIX 3 5G (andromeda), to include in custom ROM builds.

**Current version**: fw_andromeda_miui_ANDROMEDAEEAGlobal_V12.0.7.0.PEMEUXM_ebd7b385e7_9.0

### How to use?

1. Clone this repo to `vendor/xiaomi/andromeda-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/andromeda-firmware/firmware.mk
```
